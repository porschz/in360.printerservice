﻿using System;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace In360
{

    class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        static void Main(string[] args)
        {
            Uri baseAddress = new Uri("Http://localhost:8000/PrinterService");

            // Create the ServiceHost.
            using (ServiceHost host = new ServiceHost(typeof(PrinterService), baseAddress))
            {
                // Enable metadata publishing.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior
                {
                    HttpGetEnabled = true
                };
                smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
                host.Description.Behaviors.Add(smb);

                // Open the ServiceHost to start listening for messages. Since
                // no endpoints are explicitly configured, the runtime will create
                // one endpoint per base address for each service contract implemented
                // by the service.
                //  ServiceHost host = new ServiceHost(typeof(PrinterService), baseAddress);
                try
                {
                    host.Open();

                    var handle = GetConsoleWindow();

                    // Hide
                    ShowWindow(handle, SW_HIDE);

                    // Show
                    ShowWindow(handle, SW_SHOW);

                    Console.WriteLine("The service is ready at {0}", baseAddress);
                    Console.WriteLine("Press <Enter> to stop the service.");
                    Console.ReadLine();

                    // Close the ServiceHost.
                    host.Close();
                }
                catch (TimeoutException timeProblem)
                {
                    Console.WriteLine(timeProblem.Message);
                    Console.ReadLine();
                }
                catch (CommunicationException commProblem)
                {
                    Console.WriteLine(commProblem.Message);
                    Console.ReadLine();
                }
            }
        }
    }

}
