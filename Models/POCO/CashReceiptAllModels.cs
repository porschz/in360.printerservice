﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace In360.Models.POCO
{
    class CashReceiptAllModels
    {
        public long? Seq { get; set; }
        public string member_id { get; set; }
        public string receipt_no { get; set; }
        public string FullName { get; set; }
        public DateTime receipt_date { get; set; }
        public decimal? share_value { get; set; }
        public int? TypeSeq { get; set; }
        public string Descript { get; set; }
        public int? Times { get; set; }
        public decimal? amt { get; set; }
        public decimal int_amt { get; set; }
        public decimal total { get; set; }
        public decimal? cf_bal { get; set; }
        public decimal? SumTotal { get; set; }
    }
    class CashReceiptAllList
    {
        public int? Group { get; set; }
        public long? Seq { get; set; }
        public string member_id { get; set; }
    }
}
