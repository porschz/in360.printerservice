﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace In360.Models.POCO
{
    class CashReceiptModel
    {
        public int seq { get; set; }
        public string receipt_no { get; set; }
        public string MemberId { get; set; }
        public string mask_member_id { get; set; }
        public string Name { get; set; }
        public DateTime TxnDate { get; set; }
        public decimal? ShareValue { get; set; }
        public string Descript { get; set; }
        public string Id { get; set; }
        public int? InstallTimes { get; set; }
        public decimal? BfBal { get; set; }
        public decimal? IntAmt { get; set; }
        public decimal Amt { get; set; }
        public decimal? CfBal { get; set; }
    }
}
