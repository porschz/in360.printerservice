﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace In360.Models.POCO
{
    class JournalDetailModels
    {
        public string create_user_id { get; set; }
        public DateTime? create_by { get; set; }
        public string user_id { get; set; }
        public DateTime? update_by { get; set; }
        public string filestatus { get; set; }
        public string coop_id { get; set; }
        public string t_doc { get; set; }
        public string doc_no { get; set; }
        public DateTime txn_date { get; set; }
        public short seq { get; set; }
        public string ledger_no { get; set; }
        public string descript { get; set; }
        public string cd_code { get; set; }
        public double? debit { get; set; }
        public double? credit { get; set; }
        public int? Group { get; set; }
    }
}
