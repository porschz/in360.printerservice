﻿using System.Configuration;
using In360.Entities;
using In360.Models.POCO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Windows.Forms;
using System.Data.Entity;

namespace In360
{

    // Define a service contract.
    [ServiceContract(Namespace = "http://In360.PrinterService")]
    public interface IPrinterService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        void AlertA(string str);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintA();
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string Print(string req);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        void PrintBookOpen(string coopId, string strTDeposit, string strAccountNo, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        void PrintBookBF(string coopId, string strTDeposit, string strAccountNo, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintHeadBook(string coopId, string strTDeposit, string strAccountNo, string strOCFlag, ref bool bolNewPageBook, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintBook(string coopId, string strTDeposit, string strAccountNo, string strOCFlag, ref bool bolNewPageBook, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintCashReceipt(string coopId, string strMemberId, string strReceiptNo, DateTime datTxnDate);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintDepositReceipt(string coopId, string strAccountNo, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintCashReceiptMonth(string coopId, string TBatch, string TRouteStart, string TRouteEnd, string MemberIdStart, string MemberIdEnd, string BudgetYear, short AccountPeroid);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintDailyJournalReceipt(string coopId, DateTime txn_date, string doc_no, string cd_code, string ledger_no);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintHeadBookPn(string coopId, string strTPn, string strPNNo, ref bool bolNewPageBook, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintBookPn(string coopId, string strTPn, string strPNNo, ref bool bolNewPageBook, string szUserId);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintPnReceipt(string coopId, string strCustomerId, string strReceiptNo, DateTime datTxnDate);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintPayment(string coopId, string memberId, DateTime datTxnDate);
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string PrintDailyReceipt(int Id);
    }

    // Implement the IPrinter service contract in a service class.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PrinterService : IPrinterService
    {
        //ประกาศ Parameter เป็นแบบ Global เพื่อเรียกใช้ในการ Drawing
        #region "Parameter"
        static int giBookMaxLine = 24;
        static int giBookMidLine = 12;
        static double dblWidthdrawAmt = 0;
        static double dblDepositAmt = 0;
        static double dblBookBal = 0;
        static long intItemNo = 0;
        static int intLine = 0;
        static int intRowHeight = 5;
        static int intMidHeight = 0;
        static double MarginLeft = 0;
        static double MarginTop = 7;
        static string msg = "";
        static int PageStart = 0;

        static float cashReceiptWidth = 115;
        static float cashReceiptWidth2 = cashReceiptWidth * 2;
        static float cashReceiptHeight = 140;

        private string pText = "";
        private string pCoopId = "";
        private string pMemberId = "";
        private string pTDeposit = "";
        private string pAccountNo = "";
        private string pUserId = "";
        private string pOCFlag = "";
        private string pReceiptNo = "";
        private string pTBatch = "";
        private string pTRouteStart = "";
        private string pTRouteEnd = "";
        private string pMemberIdStart = "";
        private string pMemberIdEnd = "";
        private string pBudgetYear = "";
        private short pAccountPeroid = 0;
        private DateTime pTxnDate = DateTime.Now;
        private bool pNewPageBook = true;
        private int? pCashReceiptAllList;
        private List<CashReceiptAllList> pList;
        private List<JournalDetailModels> jList;
        private string pLedgerNo = "";
        private string pLedgerType = "";
        private string pCDCode = "";
        private bool pBool = false;
        private string pTPn = "";
        private string pPnNo = "";
        private string pCustomerId = "";
        private string pDocNo = "";
        private string pReceiptType = "";
        private int pId = 0;
        private string printerPassbook = ConfigurationManager.AppSettings["PrinterPassbook"];
        private string printerGL = ConfigurationManager.AppSettings["PrinterGL"];
        private string printerhReceipt = ConfigurationManager.AppSettings["PrinterhReceipt"];
        private string printerhMonthReceipt = ConfigurationManager.AppSettings["PrinterhMonthReceipt"];

        private double StartCashReceiptX = Convert.ToDouble(ConfigurationManager.AppSettings["StartCashReceiptX"]);
        private double StartCashReceiptY = Convert.ToDouble(ConfigurationManager.AppSettings["StartCashReceiptY"]);

        private static double CoordinateY(double y)
        {
            var coodinateY = y - 3;
            return coodinateY;
        }  //ตำแหน่ง Default แกน Y
        static double difference_x = 0;
        private static double CurrentX(double x)
        {
            double overdoseX = 0;
            if (difference_x == 0)
            {
                overdoseX = 0; //CurrentX Print Start
            }
            else
            {
                overdoseX = difference_x;
            }
            double cCurentX;
            cCurentX = x + overdoseX;
            return cCurentX;
        } //ตำแหน่งแกน X
        static double difference_y = 0;
        private static double CurrentY(double y)
        {
            double overdoseY = 0;
            if (difference_y == 0)
            {
                overdoseY = 3; //CurrentX Print Start
            }
            else
            {
                overdoseY = difference_y;
            }
            double cCurentY;
            cCurentY = y + overdoseY;
            return cCurentY;
        } //ตำแหน่งแกน Y
        #endregion
        //เรียกใช้คำสั่ง Print
        #region "Print"
        public void AlertA(string str)
        {
            string ptext = "Test !! " + str;
            MessageBox.Show(ptext, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        } // Test โปรแกรมเพื่อเช็คว่าเรียก Service ได้
        public string PrintA()
        {
            pText = "Test";
            var d = new PrintDocument();

            // Handle the PrintPage event to write the print logic.
            d.PrinterSettings.PrinterName = printerhReceipt;
            d.PrintPage += new PrintPageEventHandler(PrintFn);
            d.Print();
            return pText;
        }  // Test Print
        public string Print(string req)
        {
            string ptext = "Test !! ";
            MessageBox.Show(ptext, "", MessageBoxButtons.OK, MessageBoxIcon.Information);

            pCoopId = "0001";
            pMemberId = "090543";
            pReceiptNo = "63010001";
            pTxnDate = new DateTime(2019, 12, 2);

            pText = req;
            MessageBox.Show(pText, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            var d = new PrintDocument();

            // Handle the PrintPage event to write the print logic.
            d.PrinterSettings.PrinterName = printerhReceipt;
            d.PrintPage += new PrintPageEventHandler(PrintFn);
            d.Print();
            return req;
        } // Test Print And Alert
        public void PrintBookOpen(string coopId, string strTDeposit, string strAccountNo, string szUserId)
        {
            pCoopId = coopId;
            pTDeposit = strTDeposit;
            pAccountNo = strAccountNo;
            pUserId = szUserId;

            var d = new PrintDocument();
            d.PrinterSettings.PrinterName = printerPassbook;
            d.PrintPage += new PrintPageEventHandler(PrintBookOpenFn);
            d.Print();
        }  // เปิดบัญชี(ไม่ใช้แล้ว ไปใช้ PrintHeadBook)
        public void PrintBookBF(string coopId, string strTDeposit, string strAccountNo, string szUserId)
        {
            pCoopId = coopId;
            pTDeposit = strTDeposit;
            pAccountNo = strAccountNo;
            pUserId = szUserId;

            var d = new PrintDocument();
            d.PrinterSettings.PrinterName = printerPassbook;
            d.PrintPage += new PrintPageEventHandler(PrintHeadBookFn);
            d.Print();

            d = new PrintDocument();
            d.PrinterSettings.PrinterName = printerPassbook;
            d.PrintPage += new PrintPageEventHandler(PrintBookBFFn);
            d.Print();
        }   // รายการสมุดบัญชี(สรุปรวบยอดรวม)
        public string PrintHeadBook(string coopId, string strTDeposit, string strAccountNo, string strOCFlag, ref bool bolNewPageBook, string szUserId)
        {
            pCoopId = coopId;
            pTDeposit = strTDeposit;
            pAccountNo = strAccountNo;
            pUserId = szUserId;
            pOCFlag = strOCFlag;
            //pNewPageBook = bolNewPageBook;

            var d = new PrintDocument();
            d.PrinterSettings.PrinterName = printerPassbook;
            d.PrintPage += new PrintPageEventHandler(PrintHeadBookFn);
            d.Print();

            d = new PrintDocument();
            d.PrinterSettings.PrinterName = printerPassbook;
            d.PrintPage += new PrintPageEventHandler(PrintBookFn);
            d.Print();

            return "Successed";
        } //เปิดบัญชี
        public string PrintBook(string coopId, string strTDeposit, string strAccountNo, string strOCFlag, ref bool bolNewPageBook, string szUserId)
        {
            pCoopId = coopId;
            pTDeposit = strTDeposit;
            pAccountNo = strAccountNo;
            pUserId = szUserId;
            pOCFlag = strOCFlag;
            //pNewPageBook = bolNewPageBook;

            do
            {
                var d = new PrintDocument();
                d.PrinterSettings.PrinterName = printerPassbook;
                d.PrintPage += new PrintPageEventHandler(PrintBookFn);
                d.Print();

            } while (msg != "");

            return msg;
        } // รายการสมุดบัญชี
        public string PrintCashReceipt(string coopId, string strMemberId, string strReceiptNo, DateTime datTxnDate)
        {

            try
            {
                pCoopId = coopId;
                pMemberId = strMemberId;
                pReceiptNo = strReceiptNo;
                pTxnDate = datTxnDate;

                var d = new PrintDocument();
                d.DefaultPageSettings.PaperSize = new PaperSize("Custom", 930, 550);
                d.DefaultPageSettings.PaperSize.RawKind = 119;
                d.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                d.DefaultPageSettings.Landscape = false;
                d.PrinterSettings.PrinterName = printerhReceipt;
                d.PrintPage += new PrintPageEventHandler(PrintCashReceiptFn);
                d.Print();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            return msg;
        }   // ใบเสร็จคู่
        public string PrintDepositReceipt(string coopId, string strAccountNo, string szUserId)
        {
            pCoopId = coopId;
            pAccountNo = strAccountNo;
            pUserId = szUserId;

            do
            {
                //if (MessageBox.Show("กรุณาใส่ใบรับเงินสด....เข้าเครื่องพิมพ์...!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                // {
                var d = new PrintDocument();
                d.PrinterSettings.PrinterName = printerPassbook;
                d.PrintPage += new PrintPageEventHandler(PrintDepositReceiptFn);
                //d.PrinterSettings.DefaultPageSettings.PaperSize = new PaperSize("a2", 5, 5);
                d.Print();
                //}

            } while (msg != "");

            return msg;
        } // ใบฝาก-ถอน
        public string PrintCashReceiptMonth(string coopId, string TBatch, string TRouteStart, string TRouteEnd, string MemberIdStart, string MemberIdEnd, string BudgetYear, short AccountPeroid)
        {
            pCoopId = coopId;
            pTBatch = TBatch;
            pTRouteStart = TRouteStart;
            pTRouteEnd = TRouteEnd;
            pMemberIdStart = MemberIdStart;
            pMemberIdEnd = MemberIdEnd;
            pBudgetYear = BudgetYear;
            pAccountPeroid = AccountPeroid;
            if (MessageBox.Show("กรุณาใส่ใบรับเงินสด....เข้าเครื่องพิมพ์...!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            {
                using (var db = new ScoopNetEntities())
                {
                    var member = (from md in db.PrnCashReceiptAll(coopId, TBatch, TRouteStart, TRouteEnd, MemberIdStart, MemberIdEnd, BudgetYear, AccountPeroid)
                                  select new CashReceiptAllList
                                  {
                                      member_id = md.member_id,
                                      Seq = md.Seq
                                  }).Distinct().GroupBy(p => p.member_id).Select(p => p.First()).ToList();
                    var num = 1;
                    foreach (var p in member)
                    {
                        p.Group = num;
                        if (p.Seq % 6 == 0)
                        {
                            num++;
                        }
                    }
                    pList = member;
                    var eGroup = member.Select(p => p.Group).Distinct().ToList();
                    foreach (var item in eGroup)
                    {
                        pCashReceiptAllList = item;
                        var d = new PrintDocument();
                        d.PrintPage += new PrintPageEventHandler(PrintCashReceiptAllFn);
                        d.PrinterSettings.PrinterName = printerhMonthReceipt;
                        IEnumerable<PaperSize> paperSize = d.PrinterSettings.PaperSizes.Cast<PaperSize>();
                        PaperSize sizeAA = paperSize.First(size => size.Kind == PaperKind.USStandardFanfold);
                        d.DefaultPageSettings.PaperSize = sizeAA;
                        d.Print();
                    }
                }
            }

            return msg;
        }  // ใบเสร็จรายเดือน FanFold 6 รายการ
        public string PrintDailyJournalReceipt(string coopId, DateTime txn_date, string doc_no, string cd_code, string ledger_no)
        {
            pCoopId = coopId;
            pTxnDate = txn_date;
            pDocNo = doc_no;
            pLedgerNo = ledger_no;
            // if (MessageBox.Show("กรุณาใส่ใบรับเงินสด....เข้าเครื่องพิมพ์...!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            //{
            using (var db = new ScoopNetEntities())
            {
                var ledger = (from ld in db.journals
                              where ld.doc_no == doc_no
                              select ld).FirstOrDefault();
                var mainCode = ledger.t_doc == "JTR" ? "111212" : "111101";
                var head = (from h in db.journal_detail
                            where h.doc_no == ledger.doc_no && h.ledger_no != mainCode && h.cd_code == cd_code
                            select h.ledger_no).Distinct().ToList();
                var journal = (from jd in db.journal_detail
                               where jd.doc_no == ledger.doc_no
                               select new JournalDetailModels()
                               {
                                   filestatus = jd.filestatus,
                                   coop_id = jd.coop_id,
                                   t_doc = jd.t_doc,
                                   doc_no = jd.doc_no,
                                   txn_date = jd.txn_date,
                                   seq = jd.seq,
                                   ledger_no = jd.ledger_no,
                                   descript = jd.descript,
                                   cd_code = jd.cd_code,
                                   debit = jd.debit,
                                   credit = jd.credit
                               }).ToList();
                if (!string.IsNullOrWhiteSpace(ledger_no))
                {
                    head = head.Where(p => head.Contains(ledger_no)).ToList();
                }
                foreach (var j in head)
                {
                    jList = journal.Where(p => p.ledger_no == j).ToList();
                    var num = 1;
                    foreach (var p in jList)
                    {
                        p.Group = num;
                        if (p.seq % 12 == 0)
                        {
                            num++;
                        }
                    }

                    var eGroup = jList.Select(p => p.Group).Distinct().ToList();
                    foreach (var item in eGroup)
                    {
                        pCashReceiptAllList = item;
                        var d = new PrintDocument();
                        d.PrintPage += new PrintPageEventHandler(PrintDailyJournalReceiptFn);
                        d.DefaultPageSettings.PaperSize = new PaperSize("Custom", 930, 550);
                        d.DefaultPageSettings.PaperSize.RawKind = 119;
                        d.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                        d.DefaultPageSettings.Landscape = false;
                        d.PrinterSettings.PrinterName = printerGL;
                        d.Print();
                    }
                }
                //}
            }
            return "Success";
        } // ใบเสร็จรายการบัญชี
        public string PrintHeadBookPn(string coopId, string strTPn, string strPNNo, ref bool bolNewPageBook, string szUserId)
        {
            pCoopId = coopId;
            pTPn = strTPn;
            pPnNo = strPNNo;
            pUserId = szUserId;

            if (MessageBox.Show("กรุณาใส่หน้าสมุดคู่ตั๋วสัญญา...เข้าเครื่องพิมพ์...!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
            {
                var d = new PrintDocument();
                d.PrinterSettings.PrinterName = printerPassbook;
                d.PrintPage += new PrintPageEventHandler(PrintHeadBookPnFn);
                d.Print();
            }
            return "Success";
        } // เปิดตั๋วสัญญา
        public string PrintBookPn(string coopId, string strTPn, string strPNNo, ref bool bolNewPageBook, string szUserId)
        {
            pCoopId = coopId;
            pTPn = strTPn;
            pPnNo = strPNNo;
            pUserId = szUserId;
            //pNewPageBook = bolNewPageBook;

            do
            {
                if (MessageBox.Show("กรุณาใส่สมุดคู่ตั๋วสัญญาหน้าใหม่....เข้าเครื่องพิมพ์...!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    var d = new PrintDocument();
                    d.PrinterSettings.PrinterName = printerPassbook;
                    d.PrintPage += new PrintPageEventHandler(PrintBookPnFn);
                    d.Print();
                }

            } while (msg != "");

            return msg;
        } // รายการตั๋วสัญญา
        public string PrintPnReceipt(string coopId, string strCustomerId, string strReceiptNo, DateTime datTxnDate)
        {
            pCoopId = coopId;
            pCustomerId = strCustomerId;
            pReceiptNo = strReceiptNo;
            pTxnDate = datTxnDate;

            do
            {
                if (MessageBox.Show("กรุณาใส่ใบรับเงินสด....เข้าเครื่องพิมพ์...!!!", "", MessageBoxButtons.OK, MessageBoxIcon.Information) == DialogResult.OK)
                {
                    var d = new PrintDocument();
                    d.DefaultPageSettings.PaperSize = new PaperSize("Custom", 930, 550);
                    d.DefaultPageSettings.PaperSize.RawKind = 119;
                    d.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                    d.DefaultPageSettings.Landscape = false;
                    d.PrinterSettings.PrinterName = printerhReceipt;
                    d.PrintPage += new PrintPageEventHandler(PrintPnReceiptFn);
                    d.Print();
                }

            } while (msg != "");

            return msg;
        } // ใบเสร็จตั๋วสัญญา
        public string PrintPayment(string coopId, string memberId, DateTime datTxnDate)
        {
            pCoopId = coopId;
            pMemberId = memberId;
            pTxnDate = datTxnDate;
            using (var db = new ScoopNetEntities())
            {
                var pri = db.Receipts.Where(p => p.member_id == memberId && p.txn_date == datTxnDate && p.filestatus == "A").ToList();
                if (pri.Any())
                {
                    foreach (var i in pri)
                    {
                        pReceiptNo = i.receipt_no;
                        pText = i.t_type;
                        var d = new PrintDocument();
                        d.DefaultPageSettings.PaperSize = new PaperSize("Custom", 930, 550);
                        d.DefaultPageSettings.PaperSize.RawKind = 119;
                        d.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                        d.DefaultPageSettings.Landscape = false;
                        d.PrinterSettings.PrinterName = printerhReceipt;
                        d.PrintPage += new PrintPageEventHandler(PrintPaymentFn);
                        d.Print();
                        i.filestatus = "P";
                    }
                    db.SaveChanges();
                }
            }
            return msg;
        } // ใบเสร็จคู่ หน้ารับชำระ
        public string PrintDailyReceipt(int Id)
        {
            try
            {
                pId = Id;
                using (var db = new ScoopNetEntities())
                {
                    var pri = db.Receipts.Where(p => p.ReceiptID == pId).FirstOrDefault();
                    if (pri != null)
                    {
                        pReceiptNo = pri.receipt_no;
                        pReceiptType = pri.t_type;
                        pText = pri.Remark;
                        pMemberId = pri.member_id;
                        var d = new PrintDocument();
                        d.DefaultPageSettings.PaperSize = new PaperSize("Custom", 930, 550);
                        d.DefaultPageSettings.PaperSize.RawKind = 119;
                        d.PrinterSettings.DefaultPageSettings.PaperSize.RawKind = 119;
                        d.DefaultPageSettings.Landscape = false;
                        d.PrinterSettings.PrinterName = printerhReceipt;
                        d.PrintPage += new PrintPageEventHandler(PrintPaymentFn);
                        d.Print();
                        return "พิมพ์สำเร็จ";
                    }
                }
                return "พิมพ์ไม่สำเร็จ กรุณาตรวจสอบข้อมูล";
            }
            catch (Exception e)
            {
                return "เกิดข้อผิดพลาด " + e.Message;
            }
            
        }

        #endregion
        //เซ็ตตำแหน่งการ Print (Drawing)
        #region "Drawing"
        private void PrintFn(object sender, PrintPageEventArgs e)
        {
            // Specify what to print and how to print in this event handler.
            // The follow code specify a string and a rectangle to be print 
            using (Font f = new Font("Vanada", 12))
            using (SolidBrush br = new SolidBrush(Color.Black))
            using (Pen p = new Pen(Color.Black))
            {
                e.Graphics.DrawString(pText, f, br, 50, 50);

                e.Graphics.DrawRectangle(p, 50, 100, 300, 150);
            }
        }
        private void PrintBookOpenFn(object sender, PrintPageEventArgs e)
        {
            MarginTop = -10;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("MS Sans Serif", 9, FontStyle.Regular);
            var fontNum = new Font("MS Sans Serif", 9, FontStyle.Regular);
            using (Pen p = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {

                    var rsDeposit = (from d in db.deposits
                                     where d.coop_id == pCoopId && d.t_deposit == pTDeposit && d.account_no == pAccountNo && d.filestatus == "A"
                                     select d).FirstOrDefault();
                    var coop = (from c in db.coops
                                where c.coop_id == pCoopId && c.filestatus == "A"
                                select c).FirstOrDefault();

                    var rsNoBook = (from n in db.nobooks
                                    where n.coop_id == pCoopId && n.t_deposit == pTDeposit && n.account_no == pAccountNo && n.filestatus == "A"
                                    orderby n.seq
                                    select n).ToList();

                    intMidHeight = 0;

                    intLine = 1;

                    e.Graphics.DrawString(coop.system_date.Value.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(0)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString(rsNoBook.FirstOrDefault().abb_code, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsNoBook.FirstOrDefault().cf_ledger_bal.Value), fontNum);

                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsDeposit.ledger_bal), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(80) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsDeposit.ledger_bal), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(112) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString(pUserId, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(112)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    // Update Deposit New Book
                    rsDeposit.last_book_line = 1;
                    rsDeposit.book_page = 1;
                    rsDeposit.book_seq = rsDeposit.book_seq + 1;
                    rsDeposit.book_bal = rsDeposit.ledger_bal;
                    db.deposits.Attach(rsDeposit);
                    db.Entry(rsDeposit).State = EntityState.Modified;
                    db.SaveChanges();

                    // Update NoBook

                    foreach (var nb in rsNoBook)
                    {
                        var q = (from n in db.nobooks
                                 where n.filestatus == "A"
                                 && n.coop_id == nb.coop_id
                                 && n.t_deposit == nb.t_deposit
                                 && n.account_no == nb.account_no
                                 && n.seq == nb.seq
                                 select n).FirstOrDefault();
                        if (q != null)
                        {
                            q.filestatus = "P";
                            db.nobooks.Attach(q);
                            db.Entry(q).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }
        }
        private void PrintBookBFFn(object sender, PrintPageEventArgs e)
        {
            MarginTop = 0;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("MS Sans Serif", 9, FontStyle.Regular);
            var fontNum = new Font("MS Sans Serif", 9, FontStyle.Regular);
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {

                    var rsDeposit = (from p in db.deposits
                                     where p.coop_id == pCoopId && p.t_deposit == pTDeposit && p.account_no == pAccountNo && p.filestatus == "A"
                                     select p).FirstOrDefault();
                    var coop = (from p in db.coops
                                where p.coop_id == pCoopId && p.filestatus == "A"
                                select p).FirstOrDefault();

                    intMidHeight = 0;

                    intLine = 1;

                    e.Graphics.DrawString(coop.system_date.Value.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(0)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString("B/F", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsDeposit.ledger_bal), fontNum);

                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsDeposit.ledger_bal), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(110) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString(pUserId.Substring(0, 3), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(112)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                    // Update Deposit New Book
                    rsDeposit.last_book_line = 1;
                    rsDeposit.book_page = 1;
                    rsDeposit.book_seq = rsDeposit.book_seq + 1;
                    rsDeposit.book_bal = rsDeposit.ledger_bal;
                    db.deposits.Attach(rsDeposit);
                    db.Entry(rsDeposit).State = EntityState.Modified;
                    db.SaveChanges();

                    // Update NoBook
                    var rsNoBook = (from p in db.nobooks
                                    where p.coop_id == pCoopId && p.t_deposit == pTDeposit && p.account_no == pAccountNo && p.filestatus == "A"
                                    orderby p.seq
                                    select p).ToList();
                    foreach (var nb in rsNoBook)
                    {
                        var q = (from p in db.nobooks
                                 where p.filestatus == "A"
                                 && p.coop_id == nb.coop_id
                                 && p.t_deposit == nb.t_deposit
                                 && p.account_no == nb.account_no
                                 && p.seq == nb.seq
                                 select p).FirstOrDefault();
                        if (q != null)
                        {
                            q.filestatus = "P";
                            db.nobooks.Attach(q);
                            db.Entry(q).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
            }
        }
        private void PrintHeadBookFn(object sender, PrintPageEventArgs e)
        {
            MarginTop = 1;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 16, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 16, FontStyle.Regular);
            using (Pen penp = new Pen(Color.Black))
            {
                string strMemberId = "";
                string strName = "";
                string strMemberType = "";
                string strMaskAccountNo = "";
                string strMaskMemberId = "";
                using (var db = new ScoopNetEntities())
                {
                    var rsDeposit = (from d in db.deposits
                                     join td in db.t_deposit on d.t_deposit equals td.t_deposit1
                                     join m in db.members on d.member_id equals m.member_id
                                     join mt in db.MemberTypes on m.MemberTypeID equals mt.MemberTypeID
                                     where d.coop_id == pCoopId && d.t_deposit == pTDeposit && d.account_no == pAccountNo
                                     select new
                                     {
                                         MemberId = d.member_id,
                                         Name = d.account_name,
                                         MemberType = mt.descript,
                                         MaskAccountNo = td.account_no_mask,
                                         MaskMemberId = mt.mask_member_id
                                     }).FirstOrDefault();
                    if (rsDeposit != null)
                    {
                        strMemberId = rsDeposit.MemberId;
                        strName = rsDeposit.Name;
                        strMemberType = rsDeposit.MemberType;
                        strMaskAccountNo = rsDeposit.MaskAccountNo;
                        strMaskMemberId = rsDeposit.MaskMemberId;
                    };
                }

                e.Graphics.DrawString(strName, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(35)), (float)(CoordinateY(MarginTop + CurrentY(113))));

                e.Graphics.DrawString(SetMaskID(pAccountNo, strMaskAccountNo), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(35)), (float)(CoordinateY(MarginTop + CurrentY(124))));

                e.Graphics.DrawString(SetMaskID(strMemberId, strMaskMemberId) + "     " + strMemberType, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(35)), (float)(CoordinateY(MarginTop + CurrentY(135))));
            }
        }
        private void PrintBookFn(object sender, PrintPageEventArgs e)
        {
            msg = "";
            MarginTop = 0;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("MS Sans Serif", 9, FontStyle.Regular);
            var fontNum = new Font("MS Sans Serif", 9, FontStyle.Regular);
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var rsDeposit = from p in db.deposits
                                    where p.coop_id == pCoopId && p.t_deposit == pTDeposit && p.account_no == pAccountNo
                                    select p;

                    var rsNoBook = (from p in db.nobooks
                                    where p.coop_id == pCoopId && p.t_deposit == pTDeposit && p.account_no == pAccountNo && p.filestatus == "A"
                                    orderby p.seq
                                    select p).ToList();
                    var typeOfDeposit = rsDeposit.FirstOrDefault().t_deposit1.type_of_deposit;

                    intLine = Convert.ToInt32(rsDeposit.FirstOrDefault().last_book_line ?? 0);

                    foreach (var nb in rsNoBook)
                    {
                        intLine = intLine + 1;
                        if (intLine > giBookMaxLine)
                        {
                            //if (bolNewPageBook == false)
                            //{
                            //    bolNewPageBook = true;
                            //    //goto NewPrintBook;
                            //    break;
                            //}
                            pNewPageBook = false;
                            intLine = 0;
                            msg = "กรุณาใส่สมุดคู่บัญชีหน้าใหม่....เข้าเครื่องพิมพ์...!!!";
                            Console.Beep();
                            goto NewPrintBook;
                            //MsgBox("กรุณาใส่สมุดคู่บัญชีหน้าใหม่....เข้าเครื่องพิมพ์...!!!")  
                        }
                        if (intLine <= giBookMidLine)
                        {
                            intMidHeight = 0;
                        }
                        else
                        {
                            intMidHeight = 12;
                        }

                        e.Graphics.DrawString(nb.txn_date.Value.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(0)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                        e.Graphics.DrawString(nb.abb_code, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                        //if (typeOfDeposit == "FIX")
                        //{
                        //    e.Graphics.DrawString(nb.item_no.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(18) - nb.abb_code.Length), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        //}

                        if (nb.cd_code == "D")
                        {
                            var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum);

                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(56) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        }
                        else
                        {
                            var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(78) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        }

                        var TextWidth2 = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.cf_ledger_bal.Value), fontNum);

                        e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.cf_ledger_bal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(110) - TextWidth2.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        e.Graphics.DrawString(pUserId.Substring(0, 3), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(112)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                        dblBookBal = (double)nb.cf_ledger_bal;


                        var q = (from p in db.nobooks
                                 where p.filestatus == "A"
                                 && p.coop_id == pCoopId
                                 && p.t_deposit == pTDeposit
                                 && p.account_no == pAccountNo
                                 && p.seq == nb.seq
                                 select p).FirstOrDefault();
                        if (q != null)
                        {
                            q.filestatus = "P";
                            db.nobooks.Attach(q);
                            db.Entry(q).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    NewPrintBook:

                    if (pOCFlag == "C" && pNewPageBook == true)
                    {
                        e.Graphics.DrawString("--------------------- ACCOUNT CLOSED ---------------------", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(25)), (float)(CoordinateY(MarginTop + CurrentY(16) + ((intLine + 1) * intRowHeight) + intMidHeight)));
                    }

                    var deposits = (from p in db.deposits
                                    where p.coop_id == pCoopId
                                    && p.t_deposit == pTDeposit
                                    && p.account_no == pAccountNo
                                    select p).FirstOrDefault();

                    if (deposits != null)
                    {
                        deposits.last_book_line = (short)intLine;
                        deposits.book_bal = dblBookBal;
                    }
                    db.deposits.Attach(deposits);
                    db.Entry(deposits).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        private void PrintCashReceiptFn(object sender, PrintPageEventArgs e)
        {
            var StartLeft = 0 + (StartCashReceiptX);
            var StartTop = 0 + (StartCashReceiptY);
            msg = "";
            decimal sumAmt = 0;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 12, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 12, FontStyle.Regular);
            intMidHeight = 9;
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var pri = db.PrnCashReceipt(pCoopId, pMemberId, pReceiptNo, pTxnDate).ToList();
                    var header = pri.FirstOrDefault();
                    // ใบเสร็จรับเงิน
                    e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(StartLeft + CurrentX(85)), (float)(CoordinateY(StartTop + CurrentY(3) + (1 * intRowHeight) + intMidHeight) + 0.2));

                    e.Graphics.DrawString(header.Name, fontText, Brushes.Black, (float)(StartLeft + CurrentX(23)), (float)(CoordinateY(StartTop + CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.TxnDate.Value.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(StartLeft + CurrentX(85)), (float)(CoordinateY(StartTop + CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(SetMaskID(header.MemberId, header.mask_member_id), fontText, Brushes.Black, (float)(StartLeft + CurrentX(23)), (float)(CoordinateY(StartTop + CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(109) - TextWidth.Width), (float)(CoordinateY(StartTop + CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    if (!pri.Where(p => p.Descript == "ค่าหุ้น").Any())
                    {
                        pri.Insert(0, new PrnCashReceipt_Result
                        {
                            seq = header.seq,
                            MemberId = header.MemberId,
                            receipt_no = header.receipt_no,
                            Name = header.Name,
                            TxnDate = header.TxnDate,
                            ShareValue = header.ShareValue,
                            Descript = "",
                            Id = "",
                            InstallTimes = 0,
                            Credit = 0,
                            IntAmt = 0,
                            Amt = 0,
                            CfBal = 0
                        });
                    }

                    sumAmt = 0;
                    intLine = 1;
                    foreach (var ln in pri)
                    {
                        if (ln.Descript == "ค่าหุ้น")
                        {
                            if (ln.InstallTimes != null)
                                e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(StartLeft + CurrentX(27)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(92) - TextWidthAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(110) - TextWidthCfBal.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        else if (ln.Descript == "")
                        {

                        }
                        else
                        {
                            e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(StartLeft + CurrentX(2)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            if (ln.InstallTimes != null)
                                e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(StartLeft + CurrentX(28)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(54) - TextWidthCredit.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(73) - TextWidthIntAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(92) - TextWidthAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(110) - TextWidthCfBal.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        sumAmt += ln.Amt ?? 0;
                        intLine++;
                    }

                    RectangleF rectF1 = new RectangleF((float)(StartLeft + CurrentX(5)), (float)(CoordinateY(StartTop + CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                    var TextSumTH = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTH, fontText, Brushes.Black, rectF1);

                    var TextWidthsumAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(94) - TextWidthsumAmt.Width), (float)(CoordinateY(StartTop + CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1.2));
                    StartLeft = StartLeft + 113;
                    // สำเนาใบเสร็จรับเงิน
                    e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(StartLeft + CurrentX(85)), (float)(CoordinateY(StartTop + CurrentY(3) + (1 * intRowHeight) + intMidHeight) + 0.2));

                    e.Graphics.DrawString(header.Name, fontText, Brushes.Black, (float)(StartLeft + CurrentX(23)), (float)(CoordinateY(StartTop + CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.TxnDate.Value.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(StartLeft + CurrentX(85)), (float)(CoordinateY(StartTop + CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(SetMaskID(header.MemberId, header.mask_member_id), fontText, Brushes.Black, (float)(StartLeft + CurrentX(23)), (float)(CoordinateY(StartTop + CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    var TextWidthCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(109) - TextWidthCopy.Width), (float)(CoordinateY(StartTop + CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    sumAmt = 0;
                    intLine = 1;
                    foreach (var ln in pri)
                    {
                        if (ln.Descript == "ค่าหุ้น")
                        {
                            if (ln.InstallTimes != null)
                                e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(StartLeft + CurrentX(27)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(92) - TextWidthAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(117) - TextWidthCfBal.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        else if (ln.Descript == "")
                        {

                        }
                        else
                        {
                            //e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(StartLeft + CurrentX(2)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            //if (ln.InstallTimes != null)
                            //    e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(StartLeft + CurrentX(28)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            //var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                            //e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(54) - TextWidthCredit.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            //var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                            //e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(73) - TextWidthIntAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            //var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            //e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(92) - TextWidthAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            //var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            //e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(100)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(StartLeft + CurrentX(2)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            if (ln.InstallTimes != null)
                                e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(StartLeft + CurrentX(27)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(54) - TextWidthCredit.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(72) - TextWidthIntAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(91) - TextWidthAmt.Width), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(110 - TextWidthCfBal.Width)), (float)(CoordinateY(StartTop + CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        sumAmt += ln.Amt ?? 0;
                        intLine++;
                    }

                    RectangleF rectF2 = new RectangleF((float)(StartLeft + CurrentX(5)), (float)(CoordinateY(StartTop + CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                    var TextSumTHCopy = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTHCopy, fontText, Brushes.Black, rectF2);

                    var TextWidthsumAmtCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(StartLeft + CurrentX(95) - TextWidthsumAmtCopy.Width), (float)(CoordinateY(StartTop + CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1.2));
                }
            }
        }
        private void PrintDepositReceiptFn(object sender, PrintPageEventArgs e)
        {
            MarginTop = 3;
            msg = "";
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 14, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 14, FontStyle.Regular);
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var strMaskAccountNo = "";
                    var strName = "";
                    var rsDeposit = (from d in db.deposits
                                     join td in db.t_deposit on d.t_deposit equals td.t_deposit1
                                     where d.coop_id == pCoopId && d.account_no == pAccountNo
                                     select new
                                     {
                                         Name = d.account_name,
                                         MaskAccountNo = td.account_no_mask,
                                     }).FirstOrDefault();

                    if (rsDeposit != null)
                    {
                        strName = rsDeposit.Name;
                        strMaskAccountNo = rsDeposit.MaskAccountNo;
                    };

                    var rsNoBook = (from p in db.nobooks
                                    where p.coop_id == pCoopId && p.account_no == pAccountNo
                                    orderby p.seq descending
                                    select p).FirstOrDefault();

                    e.Graphics.DrawString("เลขที่บัญชี: ", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(8)), (float)(CoordinateY(MarginTop + CurrentY(20) + (1 * intRowHeight) + intMidHeight)));
                    e.Graphics.DrawString(SetMaskID(pAccountNo, strMaskAccountNo), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(24)), (float)(CoordinateY(MarginTop + CurrentY(20.2) + (1 * intRowHeight) + intMidHeight)));
                    e.Graphics.DrawString("ชื่อบัญชี: " + strName, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(50)), (float)(CoordinateY(MarginTop + CurrentY(20) + (1 * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString(rsNoBook.txn_date.Value.ToString("dd/MM/yyyy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5)), (float)(CoordinateY(MarginTop + CurrentY(20) + (3 * intRowHeight) + intMidHeight)));
                    e.Graphics.DrawString(rsNoBook.abb_code, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30)), (float)(CoordinateY(MarginTop + CurrentY(20) + (3 * intRowHeight) + intMidHeight)));

                    if (rsNoBook.cd_code == "C")
                    {
                        e.Graphics.DrawString("ฝาก", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(69)), (float)(CoordinateY(MarginTop + CurrentY(20) + (2 * intRowHeight) + intMidHeight)));
                    }
                    else
                    {
                        e.Graphics.DrawString("ถอน", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(102)), (float)(CoordinateY(MarginTop + CurrentY(20) + (2 * intRowHeight) + intMidHeight)));
                    }
                    e.Graphics.DrawString("คงเหลือ", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(135)), (float)(CoordinateY(MarginTop + CurrentY(20) + (2 * intRowHeight) + intMidHeight)));

                    if (rsNoBook.cd_code == "C")
                    {
                        var AmtVal = String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsNoBook.txn_amt);
                        var TextAmtVal = ("*****************").Substring(AmtVal.Length) + AmtVal;
                        var TextWidth = e.Graphics.MeasureString(TextAmtVal, fontNum);
                        e.Graphics.DrawString(TextAmtVal, fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(77) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(20) + (3 * intRowHeight) + intMidHeight)));
                    }
                    else
                    {
                        var AmtVal = String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsNoBook.txn_amt);
                        var TextAmtVal = ("*****************").Substring(AmtVal.Length) + AmtVal;
                        var TextWidth = e.Graphics.MeasureString(TextAmtVal, fontNum);
                        e.Graphics.DrawString(TextAmtVal, fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(111) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(20) + (3 * intRowHeight) + intMidHeight)));
                    }

                    var CFVal = String.Format(CultureInfo.InvariantCulture, "{0:N2}", rsNoBook.cf_ledger_bal);
                    var TextCFVal = ("*****************").Substring(CFVal.Length) + CFVal;
                    var TextCFWidth = e.Graphics.MeasureString(TextCFVal, fontNum);
                    e.Graphics.DrawString(TextCFVal, fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(148) - TextCFWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(20) + (3 * intRowHeight) + intMidHeight)));

                    e.Graphics.DrawString(rsNoBook.user_id, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(155)), (float)(CoordinateY(MarginTop + CurrentY(20) + (3 * intRowHeight) + intMidHeight)));
                }
            }
        }
        private void PrintCashReceiptAllFn(object sender, PrintPageEventArgs e)
        {
            msg = "";
            decimal sumAmt = 0;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 12, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 12, FontStyle.Regular);
            intMidHeight = 8;
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var re = pList.Where(p => p.Group == pCashReceiptAllList).ToList();
                    foreach (var item in re)
                    {
                        var pri = db.PrnCashReceiptAll(pCoopId, pTBatch, pTRouteStart, pTRouteEnd, pMemberIdStart, pMemberIdEnd, pBudgetYear, pAccountPeroid).Where(p => p.member_id == item.member_id).ToList();
                        var header = pri.FirstOrDefault();
                        float LineLeft = 0;
                        float LineTop = 0;
                        switch (header.Seq % 6)
                        {
                            case 1:
                                break;
                            case 2:
                                LineLeft = cashReceiptWidth;
                                break;
                            case 3:
                                LineLeft = cashReceiptWidth2;
                                break;
                            case 4:
                                LineTop = cashReceiptHeight;

                                break;
                            case 5:
                                LineLeft = cashReceiptWidth;
                                LineTop = cashReceiptHeight;
                                break;
                            case 0:
                                LineLeft = cashReceiptWidth2;
                                LineTop = cashReceiptHeight;
                                break;
                        }
                        LineLeft += 5;
                        e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(83) + LineLeft), (float)(CoordinateY(CurrentY(3) + LineTop + (1 * intRowHeight) + intMidHeight) + 0.2));

                        e.Graphics.DrawString(header.FullName, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23) + LineLeft), (float)(CoordinateY(CurrentY(4) + LineTop + (2 * intRowHeight) + intMidHeight) + 0.8));

                        e.Graphics.DrawString(header.receipt_date.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(83) + LineLeft), (float)(CoordinateY(CurrentY(4) + LineTop + (2 * intRowHeight) + intMidHeight) + 0.8));

                        e.Graphics.DrawString(header.member_id, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23) + LineLeft), (float)(CoordinateY(CurrentY(5) + LineTop + (3 * intRowHeight) + intMidHeight) + 1.2));

                        var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.share_value.Value), fontNum);
                        e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.share_value.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(109) + LineLeft - TextWidth.Width), (float)(CoordinateY(CurrentY(5) + LineTop + (3 * intRowHeight) + intMidHeight) + 1.2));

                        sumAmt = 0;
                        intLine = 1;
                        if (!pri.Where(p => p.Descript == "ค่าหุ้น").Any())
                        {
                            pri.Insert(0, new PrnCashReceiptAll_Result
                            {
                                Seq = header.Seq,
                                member_id = header.member_id,
                                receipt_no = header.receipt_no,
                                FullName = header.FullName,
                                receipt_date = header.receipt_date,
                                share_value = header.share_value,
                                TypeSeq = 0,
                                Descript = "",
                                Times = 0,
                                amt = 0,
                                int_amt = 0,
                                total = 0,
                                cf_bal = 0
                            });
                        }
                        foreach (var ln in pri)
                        {
                            if (ln.Descript == "ค่าหุ้น")
                            {
                                e.Graphics.DrawString(ln.Times.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30) + LineLeft), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.total), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.total), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(93) + LineLeft - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.cf_bal.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.cf_bal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(111.5) + LineLeft - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);
                            }
                            else if (ln.Descript == "")
                            {

                            }
                            else if (ln.Descript == "ประกันชีวิตกลุ่ม" || ln.Descript == "ประกันเงินกู้" || ln.Descript == "ค่าธรรมเนียม")
                            {
                                e.Graphics.DrawString(ln.Descript, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5) + LineLeft), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);
                                var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.total), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.total), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(93) + LineLeft - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);
                            }
                            else
                            {
                                e.Graphics.DrawString(ln.Descript, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5) + LineLeft), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                e.Graphics.DrawString(ln.Times.Value.ToString() == "0" ? "" : ln.Times.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30) + LineLeft), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.amt.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.amt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(55) + LineLeft - TextWidthCredit.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.int_amt), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.int_amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(74) + LineLeft - TextWidthIntAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.total), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.total), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(93) + LineLeft - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);

                                var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.cf_bal.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.cf_bal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(111.5) + LineLeft - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2) + LineTop);
                            }
                            sumAmt += ln.total ?? 0;
                            intLine++;
                        }
                        RectangleF rectF1 = new RectangleF((float)(MarginLeft + CurrentX(5) + LineLeft), (float)(CoordinateY(CurrentY(81) + LineTop + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                        var TextSumTH = db.ConvertBathThai(sumAmt).FirstOrDefault();
                        e.Graphics.DrawString(TextSumTH, fontText, Brushes.Black, rectF1);

                        var TextWidthsumAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                        e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(94.5) + LineLeft - TextWidthsumAmt.Width), (float)(CoordinateY(CurrentY(81) + LineTop + (1 * intRowHeight) + intMidHeight) + 1.2));
                    }
                }
            }
        }
        private void PrintDailyJournalReceiptFn(object sender, PrintPageEventArgs e)
        {
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("AngsanaUPC", 16, FontStyle.Regular);
            var fontNum = new Font("AngsanaUPC", 16, FontStyle.Regular);
            intMidHeight = 8;
            MarginTop = 9;
            cashReceiptWidth = 110;
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var head = jList.Where(p => p.Group == pCashReceiptAllList).FirstOrDefault();
                    var detail = jList.Where(p => p.Group == pCashReceiptAllList).ToList();
                    var ledgername = db.ledgers.FirstOrDefault(p => p.ledger_no == head.ledger_no).ledger_name;
                    var ReceiptNo = "เลขที่เอกสาร " + head.doc_no;
                    var DocType = "";
                    switch (head.t_doc)
                    {
                        case "JCR":
                            DocType = " รายการเงินสดจ่าย";
                            break;
                        case "JDR":
                            DocType = " รายการเงินสดรับ";
                            break;
                        case "JTR":
                            DocType = " รายการเงินโอน";
                            break;
                    }
                    var TxnDateString = SetFullDate(pTxnDate);
                    e.Graphics.DrawString(ReceiptNo + DocType, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(40)), (float)(CoordinateY(MarginTop + CurrentY(7.5))));
                    e.Graphics.DrawString(TxnDateString, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(140)), (float)(CoordinateY(MarginTop + CurrentY(7.5))));
                    e.Graphics.DrawString(head.ledger_no + " " + ledgername, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(40)), (float)(CoordinateY(MarginTop + CurrentY(16))));
                    decimal sumAmt = 0;
                    intLine = 1;
                    foreach (var item in detail)
                    {
                        decimal Amount = item.cd_code == "C" ? (decimal)item.credit : (decimal)item.debit;
                        e.Graphics.DrawString(item.descript, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30)), (float)(CoordinateY(CurrentY(20) + (intLine * intRowHeight) + intMidHeight) + intLine));
                        var TextAmount = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", Amount), fontNum);
                        e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", Amount), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(75) + cashReceiptWidth - TextAmount.Width), (float)(CoordinateY(CurrentY(20) + (intLine * intRowHeight) + intMidHeight) + intLine));
                        sumAmt += Amount;
                        intLine++;
                    }
                    //RectangleF rectF2 = new RectangleF((float)(MarginLeft + CurrentX(20)), (float)(CoordinateY(CurrentY(101) + (1 * intRowHeight) + intMidHeight)), 100, 15);
                    var TextSumTHCopy = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTHCopy, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(20)), (float)(CoordinateY(CurrentY(101) + (1 * intRowHeight) + intMidHeight)));

                    var TextWidthsumAmtCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(75) + cashReceiptWidth - TextWidthsumAmtCopy.Width), (float)(CoordinateY(CurrentY(101) + (1 * intRowHeight) + intMidHeight)));
                    pBool = true;
                }
            }
        }
        private void PrintHeadBookPnFn(object sender, PrintPageEventArgs e)
        {
            MarginTop = 1;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 16, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 16, FontStyle.Regular);
            using (Pen penp = new Pen(Color.Black))
            {
                string strCustomerId = "";
                string strName = "";
                string strMaskPNNo = "";
                using (var db = new ScoopNetEntities())
                {
                    var rsPN = (from p in db.pns
                                join tp in db.t_pn on p.t_pn equals tp.t_pn1
                                join pc in db.pn_customer on p.pn_no equals pc.pn_no
                                join c in db.customers on pc.customer_id equals c.customer_id
                                join m in db.members on c.member_id equals m.member_id
                                join ti in db.Titles on m.TitleID equals ti.TitleID
                                where p.coop_id == pCoopId && p.t_pn == pTPn && p.pn_no == pPnNo
                                select new
                                {
                                    CustomerId = pc.customer_id,
                                    Name = ti.TitleName + m.name,
                                    MaskPNNo = tp.mask_of_pn_no
                                }).FirstOrDefault();
                    if (rsPN != null)
                    {
                        strCustomerId = rsPN.CustomerId;
                        strName = rsPN.Name;
                        strMaskPNNo = rsPN.MaskPNNo;
                    };
                }
                e.Graphics.DrawString(strName, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(35)), (float)(CoordinateY(MarginTop + CurrentY(113))));

                e.Graphics.DrawString(SetMaskID(pPnNo, strMaskPNNo), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(35)), (float)(CoordinateY(MarginTop + CurrentY(124))));

                e.Graphics.DrawString(strCustomerId, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(35)), (float)(CoordinateY(MarginTop + CurrentY(135))));
            }
        }
        private void PrintBookPnFn(object sender, PrintPageEventArgs e)
        {
            MarginTop = 0;
            msg = "";
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("MS Sans Serif", 9, FontStyle.Regular);
            var fontNum = new Font("MS Sans Serif", 9, FontStyle.Regular);
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var rsPN = from p in db.pns
                               where p.coop_id == pCoopId && p.t_pn == pTPn && p.pn_no == pPnNo
                               select p;

                    var rsNoBook = (from p in db.pn_nobook
                                    where p.coop_id == pCoopId && p.t_pn == pTPn && p.pn_no == pPnNo && p.filestatus == "A"
                                    orderby p.seq
                                    select p).ToList();

                    intLine = Convert.ToInt32(rsPN.FirstOrDefault().last_book_line ?? 0);

                    foreach (var nb in rsNoBook)
                    {
                        intLine = intLine + 1;
                        if (intLine > giBookMaxLine)
                        {
                            pNewPageBook = false;
                            intLine = 0;
                            msg = "กรุณาใส่สมุดคู่บัญชีหน้าใหม่....เข้าเครื่องพิมพ์...!!!";
                            Console.Beep();
                            goto NewPrintBook;
                        }
                        if (intLine <= giBookMidLine)
                        {
                            intMidHeight = 0;
                        }
                        else
                        {
                            intMidHeight = 12;
                        }

                        e.Graphics.DrawString(nb.txn_date.Value.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(0)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                        e.Graphics.DrawString(nb.abb_code, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                        if (nb.cd_code == "D")
                        {
                            var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum);

                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(56) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        }
                        else
                        {
                            var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.txn_amt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(78) - TextWidth.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        }

                        var TextWidth2 = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.cf_ledger_bal.Value), fontNum);

                        e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", nb.cf_ledger_bal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(110) - TextWidth2.Width), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));
                        e.Graphics.DrawString(pUserId.Substring(0, 3), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(112)), (float)(CoordinateY(MarginTop + CurrentY(16) + (intLine * intRowHeight) + intMidHeight)));

                        dblBookBal = (double)nb.cf_ledger_bal;


                        var q = (from p in db.pn_nobook
                                 where p.filestatus == "A"
                                 && p.coop_id == pCoopId
                                 && p.t_pn == pTPn
                                 && p.pn_no == pPnNo
                                 && p.seq == nb.seq
                                 select p).FirstOrDefault();
                        if (q != null)
                        {
                            q.filestatus = "P";
                            db.pn_nobook.Attach(q);
                            db.Entry(q).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }

                    NewPrintBook:
                    var last = rsNoBook.OrderByDescending(p => p.seq).FirstOrDefault();
                    var OCFlag = (from p in db.t_txn_code
                                  where p.t_txn_code1 == last.t_txn_code
                                  select p.oc_flag).FirstOrDefault();

                    if (OCFlag == "C" && pNewPageBook == true)
                    {
                        e.Graphics.DrawString("--------------------- PN CLOSED ---------------------", fontText, Brushes.Black, (float)(MarginLeft + CurrentX(25)), (float)(CoordinateY(MarginTop + CurrentY(16) + ((intLine + 1) * intRowHeight) + intMidHeight)));
                    }

                    var pn = (from p in db.pns
                              where p.coop_id == pCoopId
                              && p.t_pn == pTPn
                              && p.pn_no == pPnNo
                              select p).FirstOrDefault();

                    if (pn != null)
                    {
                        pn.last_book_line = (short)intLine;
                        pn.book_bal = (decimal)dblBookBal;
                    }
                    db.pns.Attach(pn);
                    db.Entry(pn).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
        }
        public string SetMaskID(string id, string mask)
        {
            var cL = 0;
            var idOnMask = "";
            for (var i = 0; i < mask.Length; i++)
            {
                if (mask.Substring(i, 1) == "-" || mask.Substring(i, 1) == "/" || mask.Substring(i, 1) == ".")
                {
                    idOnMask += mask.Substring(i, 1);
                }
                else
                {
                    idOnMask += id.Substring(cL, 1);
                    cL++;
                }

            }
            return idOnMask;
        }
        public string FillZero(string dataIn, int length)
        {
            string strIn;
            strIn = dataIn.Trim().Replace("-", "");
            for (int i = strIn.Length; i < length; i++)
            {
                strIn = "0" + strIn;
            }
            return strIn;
        }
        public string SetFullDate(DateTime Date)
        {
            var StrDate = Date.Day.ToString();
            var Month = "";
            switch (Date.Month)
            {
                case 1:
                    Month = "มกราคม";
                    break;
                case 2:
                    Month = "กุมภาพันธ์";
                    break;
                case 3:
                    Month = "มีนาคม";
                    break;
                case 4:
                    Month = "เมษายน";
                    break;
                case 5:
                    Month = "พฤษภาคม";
                    break;
                case 6:
                    Month = "มิถุนายน";
                    break;
                case 7:
                    Month = "กรกฎาคม";
                    break;
                case 8:
                    Month = "สิงหาคม";
                    break;
                case 9:
                    Month = "กันยายน";
                    break;
                case 10:
                    Month = "ตุลาคม";
                    break;
                case 11:
                    Month = "พฤศจิกายน";
                    break;
                case 12:
                    Month = "ธันวาคม";
                    break;
            }
            StrDate += " " + Month + " พ.ศ.";
            StrDate += (Date.Year + 543).ToString();
            return StrDate;
        }
        private void PrintPnReceiptFn(object sender, PrintPageEventArgs e)
        {
            msg = "";
            decimal sumAmt = 0;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 12, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 12, FontStyle.Regular);
            intMidHeight = 8;
            cashReceiptWidth = cashReceiptWidth - 5;
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var pri = db.PrnPnReceipt(pCoopId, pCustomerId, pReceiptNo, pTxnDate).ToList();
                    var header = pri.FirstOrDefault();
                    // ใบเสร็จรับเงิน
                    e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(83)), (float)(CoordinateY(CurrentY(3) + (1 * intRowHeight) + intMidHeight) + 0.2));

                    e.Graphics.DrawString(header.Name, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.TxnDate.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(83)), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.CustomerId, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(109) - TextWidth.Width), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    if (!pri.Where(p => p.Descript == "ค่าหุ้น").Any())
                    {
                        pri.Insert(0, new PrnPnReceipt_Result
                        {
                            CustomerId = header.CustomerId,
                            receipt_no = header.receipt_no,
                            Name = header.Name,
                            TxnDate = header.TxnDate,
                            ShareValue = header.ShareValue,
                            Id = "",
                            Credit = 0,
                            IntAmt = 0,
                            Amt = 0,
                            CfBal = 0
                        });
                    }

                    sumAmt = 0;
                    intLine = 1;
                    foreach (var ln in pri)
                    {
                        if (ln.Id == "")
                        {

                        }
                        else
                        {
                            e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5)), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(55) - TextWidthCredit.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(74) - TextWidthIntAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(114) - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        sumAmt += ln.Amt;
                        intLine++;
                    }

                    RectangleF rectF1 = new RectangleF((float)(MarginLeft + CurrentX(5)), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                    var TextSumTH = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTH, fontText, Brushes.Black, rectF1);

                    var TextWidthsumAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) - TextWidthsumAmt.Width), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1.2));

                    // สำเนาใบเสร็จรับเงิน
                    e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(83) + cashReceiptWidth), (float)(CoordinateY(CurrentY(3) + (1 * intRowHeight) + intMidHeight) + 0.2));

                    e.Graphics.DrawString(header.Name, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23) + cashReceiptWidth), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.TxnDate.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(83) + cashReceiptWidth), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.CustomerId, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23) + cashReceiptWidth), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    var TextWidthCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(109) + cashReceiptWidth - TextWidthCopy.Width), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    sumAmt = 0;
                    intLine = 1;
                    foreach (var ln in pri)
                    {
                        if (ln.Id == "")
                        {

                        }
                        else
                        {
                            e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5) + cashReceiptWidth), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(55) + cashReceiptWidth - TextWidthCredit.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(74) + cashReceiptWidth - TextWidthIntAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) + cashReceiptWidth - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(114) + cashReceiptWidth - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        sumAmt += ln.Amt;
                        intLine++;
                    }

                    RectangleF rectF2 = new RectangleF((float)(MarginLeft + CurrentX(5) + cashReceiptWidth), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                    var TextSumTHCopy = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTHCopy, fontText, Brushes.Black, rectF2);

                    var TextWidthsumAmtCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) + cashReceiptWidth - TextWidthsumAmtCopy.Width), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1.2));
                }
            }
        }
        private void PrintPaymentFn(object sender, PrintPageEventArgs e)
        {
            MarginLeft = 0;
            msg = "";
            decimal sumAmt = 0;
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            var fontText = new Font("CordiaUPC", 12, FontStyle.Regular);
            var fontNum = new Font("CordiaUPC", 12, FontStyle.Regular);
            intMidHeight = 9;
            cashReceiptWidth = 110;
            using (Pen pen = new Pen(Color.Black))
            {
                using (var db = new ScoopNetEntities())
                {
                    var pri = db.PrintPaymentReceipt(pCoopId, pReceiptNo, pReceiptType, pText, pMemberId).ToList();
                    var header = pri.FirstOrDefault();
                    // ใบเสร็จรับเงิน
                    e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(85)), (float)(CoordinateY(CurrentY(3) + (1 * intRowHeight) + intMidHeight) + 0.2));

                    e.Graphics.DrawString(header.Name, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.TxnDate.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(85)), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(SetMaskID(header.MemberId, header.mask_member_id), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23)), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    var TextWidth = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(109) - TextWidth.Width), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    if (!pri.Where(p => p.Descript == "ค่าหุ้น").Any())
                    {
                        pri.Insert(0, new PrintPaymentReceipt_Result
                        {
                            seq = header.seq,
                            MemberId = header.MemberId,
                            receipt_no = header.receipt_no,
                            Name = header.Name,
                            TxnDate = header.TxnDate,
                            ShareValue = header.ShareValue,
                            Descript = "",
                            Id = "",
                            InstallTimes = 0,
                            Credit = 0,
                            IntAmt = 0,
                            Amt = 0,
                            CfBal = 0
                        });
                    }

                    sumAmt = 0;
                    intLine = 1;
                    foreach (var ln in pri)
                    {
                        if (ln.Descript == "ค่าหุ้น")
                        {
                            e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30)), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(114) - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        else if (ln.Descript == "")
                        {

                        }
                        else
                        {
                            e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5)), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                            if (pText == "LON")
                            {
                                e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30)), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                                var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(55) - TextWidthCredit.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                                var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(74) - TextWidthIntAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            }

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                            if (pText == "LON")
                            {
                                var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(114) - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                            }
                        }
                        sumAmt += ln.Amt;
                        intLine++;
                    }

                    RectangleF rectF1 = new RectangleF((float)(MarginLeft + CurrentX(5)), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                    var TextSumTH = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTH, fontText, Brushes.Black, rectF1);

                    var TextWidthsumAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) - TextWidthsumAmt.Width), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1.2));
                    MarginLeft = MarginLeft + 3;
                    // สำเนาใบเสร็จรับเงิน
                    e.Graphics.DrawString(header.receipt_no, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(85) + cashReceiptWidth), (float)(CoordinateY(CurrentY(3) + (1 * intRowHeight) + intMidHeight) + 0.2));

                    e.Graphics.DrawString(header.Name, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23) + cashReceiptWidth), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(header.TxnDate.ToString("dd/MM/yy"), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(85) + cashReceiptWidth), (float)(CoordinateY(CurrentY(4) + (2 * intRowHeight) + intMidHeight) + 0.8));

                    e.Graphics.DrawString(SetMaskID(header.MemberId, header.mask_member_id), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(23) + cashReceiptWidth), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    var TextWidthCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", header.ShareValue.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(109) + cashReceiptWidth - TextWidthCopy.Width), (float)(CoordinateY(CurrentY(5) + (3 * intRowHeight) + intMidHeight) + 1.2));

                    sumAmt = 0;
                    intLine = 1;
                    foreach (var ln in pri)
                    {
                        if (ln.Descript == "ค่าหุ้น")
                        {
                            e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30) + cashReceiptWidth), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) + cashReceiptWidth - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(114) + cashReceiptWidth - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                        }
                        else if (ln.Descript == "")
                        {

                        }
                        else
                        {
                            if (pText == "LON")
                            {
                                e.Graphics.DrawString(ln.Id, fontText, Brushes.Black, (float)(MarginLeft + CurrentX(5) + cashReceiptWidth), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                                e.Graphics.DrawString(ln.InstallTimes.Value.ToString(), fontText, Brushes.Black, (float)(MarginLeft + CurrentX(30) + cashReceiptWidth), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                                var TextWidthCredit = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Credit.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(55) + cashReceiptWidth - TextWidthCredit.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                                var TextWidthIntAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.IntAmt.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(74) + cashReceiptWidth - TextWidthIntAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                            }

                            var TextWidthAmt = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum);
                            e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.Amt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) + cashReceiptWidth - TextWidthAmt.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));

                            if (pText == "LON")
                            {
                                var TextWidthCfBal = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum);
                                e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", ln.CfBal.Value), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(114) + cashReceiptWidth - TextWidthCfBal.Width), (float)(CoordinateY(CurrentY(33) + (intLine * intRowHeight) + intMidHeight) + 1.2));
                            }
                        }
                        sumAmt += ln.Amt;
                        intLine++;
                    }

                    RectangleF rectF2 = new RectangleF((float)(MarginLeft + CurrentX(5) + cashReceiptWidth), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1), 58, 10);
                    var TextSumTHCopy = db.ConvertBathThai(sumAmt).FirstOrDefault();
                    e.Graphics.DrawString(TextSumTHCopy, fontText, Brushes.Black, rectF2);

                    var TextWidthsumAmtCopy = e.Graphics.MeasureString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum);
                    e.Graphics.DrawString(String.Format(CultureInfo.InvariantCulture, "{0:N2}", sumAmt), fontNum, Brushes.Black, (float)(MarginLeft + CurrentX(95) + cashReceiptWidth - TextWidthsumAmtCopy.Width), (float)(CoordinateY(CurrentY(81) + (1 * intRowHeight) + intMidHeight) + 1.2));
                }
            }
        }
        #endregion
    }
}