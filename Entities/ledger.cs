//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace In360.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class ledger
    {
        public string create_user_id { get; set; }
        public Nullable<System.DateTime> create_by { get; set; }
        public string user_id { get; set; }
        public Nullable<System.DateTime> update_by { get; set; }
        public string filestatus { get; set; }
        public string coop_id { get; set; }
        public string ledger_no { get; set; }
        public string ledger_name { get; set; }
        public string english_name { get; set; }
        public string t_ledger { get; set; }
        public string master_ledger_no { get; set; }
        public Nullable<int> subsidiary_status { get; set; }
        public Nullable<short> print_level { get; set; }
        public Nullable<decimal> bf_ledger_bal { get; set; }
        public Nullable<decimal> ledger_bal { get; set; }
        public string nature_cd { get; set; }
        public string ledger_group { get; set; }
    }
}
