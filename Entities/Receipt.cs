//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace In360.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Receipt
    {
        public int ReceiptID { get; set; }
        public string member_id { get; set; }
        public string t_type { get; set; }
        public string receipt_no { get; set; }
        public string filestatus { get; set; }
        public System.DateTime txn_date { get; set; }
        public string Remark { get; set; }
    }
}
