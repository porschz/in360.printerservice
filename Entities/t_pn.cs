//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace In360.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class t_pn
    {
        public string create_user_id { get; set; }
        public Nullable<System.DateTime> create_by { get; set; }
        public string user_id { get; set; }
        public Nullable<System.DateTime> update_by { get; set; }
        public string filestatus { get; set; }
        public string coop_id { get; set; }
        public string t_pn1 { get; set; }
        public string descript { get; set; }
        public Nullable<decimal> min_amt { get; set; }
        public Nullable<decimal> max_amt { get; set; }
        public Nullable<int> last_pn_no { get; set; }
        public string mask_of_pn_no { get; set; }
        public string t_int { get; set; }
        public Nullable<decimal> tax { get; set; }
        public Nullable<int> month_int_due { get; set; }
        public string gl_ledger_no { get; set; }
        public string gl_int_ledger_no { get; set; }
    }
}
